package odf;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.Nullable;

public class StyleNames {

	@Nullable
	final private StyleNames styles;

	final private Map<String, Set<String>> common = new HashMap<String, Set<String>>();
	final private Map<String, Set<String>> automatic = new HashMap<String, Set<String>>();

	StyleNames() {
		this.styles = null;
	}

	StyleNames(@Nullable StyleNames styles) {
		this.styles = styles;
	}

	boolean addName(String name, String family, boolean automaticStyle) {
		Map<String, Set<String>> m = automaticStyle ? automatic : common;
		Set<String> s = m.get(family);
		if (s == null) {
			s = new HashSet<String>();
			m.put(family, s);
		}
		boolean has = s.contains(name);
		s.add(name);
		return has;
	}

	boolean hasCommonStyle(String name, String family) {
		Set<String> s = common.get(family);
		return s == null ? false : s.contains(name);
	}

	boolean hasStyle(String name, String family) {
		Set<String> s = common.get(family);
		if (s != null && s.contains(name)) {
			return true;
		}
		s = automatic.get(family);
		if (s != null && s.contains(name)) {
			return true;
		}
		if (styles != null) {
			return styles.hasCommonStyle(name, family);
		}
		return false;
	}

}
