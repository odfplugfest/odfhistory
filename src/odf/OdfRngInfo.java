package odf;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.jdt.annotation.Nullable;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class OdfRngInfo {

	final HashMap<QName, LinkedList<ElementInfo>> elements;
	final HashMap<QName, LinkedList<AttributeInfo>> attributes;

	OdfRngInfo(Document grammar, XML xml) throws IOException {
		elements = new HashMap<QName, LinkedList<ElementInfo>>();
		attributes = new HashMap<QName, LinkedList<AttributeInfo>>();
		OdfRngInfoParser.handleIncludes(grammar, xml);
		OdfRngInfoParser.parse(grammar, elements, attributes);
	}

	/**
	 * Obtain the QNames for elements which are known to not contain any text
	 * nodes. If one of more definitions with the same QName allows text nodes,
	 * that QName is not returned.
	 */
	HashSet<QName> getElementsWithNoText() {
		HashSet<QName> set = new HashSet<QName>();
		for (Entry<QName, LinkedList<ElementInfo>> e : elements.entrySet()) {
			LinkedList<ElementInfo> l = e.getValue();
			boolean notext = true;
			for (ElementInfo ei : l) {
				notext = notext && !ei.text && ei.data.size() == 0;
			}
			if (notext) {
				set.add(e.getKey());
				if (e.getKey().toString().contains("creator"))
					System.out.println(e.getKey());
			}
		}
		return set;
	}

	/**
	 * Return all QNames for attributes for which there is a <data/> element
	 * with the given pattern.
	 */
	HashSet<QName> getStringAttributesWithPattern(String pattern) {
		HashSet<QName> set = new HashSet<QName>();
		for (Entry<QName, LinkedList<AttributeInfo>> e : attributes.entrySet()) {
			LinkedList<AttributeInfo> l = e.getValue();
			for (AttributeInfo ai : l) {
				for (Data d : ai.data) {
					if ("string".equals(d.type)
							&& pattern.equals(d.params.get("pattern"))) {
						set.add(e.getKey());
					}
				}
			}
		}
		return set;
	}
}

class OdfRngInfoParser {
	/**
	 * Return the first child node of @param e that is an Element. If there is
	 * no child element, return null.
	 */
	static private @Nullable Element getFirstElementChild(Element e) {
		Node n = e.getFirstChild();
		while (n != null && n.getNodeType() != Node.ELEMENT_NODE) {
			n = n.getNextSibling();
		}
		return (Element) n;
	}

	/**
	 * Return the next sibling child node of @param e that is an Element. If
	 * there is no child element, return null.
	 */
	static private @Nullable Element getNextElementSibling(Element e) {
		Node n = e.getNextSibling();
		while (n != null && n.getNodeType() != Node.ELEMENT_NODE) {
			n = n.getNextSibling();
		}
		return (Element) n;
	}

	/**
	 * Extract names for an element or attribute from the RNG grammar. This code
	 * cannot deal with all situations. It can handle a 'name' attribute and a
	 * <choice> element with names inside. <nsName/> is not supported.
	 * <anyName/> gives an empty string as name.
	 */
	static private TreeSet<String> getNames(Element element) {
		TreeSet<String> names = new TreeSet<String>();
		if (element.hasAttribute("name")) {
			names.add(element.getAttribute("name"));
		} else {
			XPathResult<String> ns = XPath.stringIterator(element,
					"rng:choice/rng:name/node()");
			if (ns.size() > 0) {
				for (final String n : ns) {
					names.add(n);
					if (n.length() == 0) {
						System.err.println("empty name!");
					}
				}
			} else {
				Element f = getFirstElementChild(element);
				if (f != null && f.getLocalName().equals("anyName")
						&& f.getNamespaceURI().equals(NC.rng)) {
					names.add("");
				} else {
					if (f != null)
						System.err.println(getFirstElementChild(f));
					throw new RuntimeException("not implemented");
				}
			}
		}
		return names;
	}

	/**
	 * Move all child nodes in source to target
	 */
	static private void appendContents(Element source, Element target) {
		while (source.getFirstChild() != null) {
			target.appendChild(source.getFirstChild());
		}
	}

	/**
	 * Return a map with all <define/> elements. The key in the map is the
	 * 'name' attribute. If there are <define/> elements with the same name,
	 * they are combined according to the rule in the 'combine' attribute.
	 */
	static HashMap<String, Element> getDefinitions(Document grammar) {
		HashMap<String, Element> defs = new HashMap<String, Element>();
		XPathResult<Element> elements = XPath.elementIterator(grammar,
				"/rng:grammar/rng:define");
		for (Element e : elements) {
			final String name = e.getAttribute("name");
			final Element def = defs.get(name);
			if (def == null) {
				defs.put(name, e);
			} else {
				final String combineMethod = def.hasAttribute("combine") ? def
						.getAttribute("combine") : e.getAttribute("combine");
				if (!"choice".equals(combineMethod)
						&& !"interleave".equals(combineMethod)) {
					throw new RuntimeException(
							"combine attribute has wrong value '"
									+ combineMethod + "' on definition '"
									+ name + "'.");
				}
				Element combine = getFirstElementChild(def);
				if (combine != null && isRng(combineMethod, combine)) {
					appendContents(e, combine);
				} else {
					combine = getFirstElementChild(e);
					if (combine != null && isRng(combineMethod, combine)) {
						appendContents(def, combine);
					} else {
						combine = e.getOwnerDocument().createElementNS(NC.rng,
								def.getPrefix() + ":" + combineMethod);
						appendContents(def, combine);
						appendContents(e, combine);
					}
					def.appendChild(combine);
				}
				def.removeAttribute("combine");
				e.getParentNode().removeChild(e);
			}
		}
		return defs;
	}

	/**
	 * Determine if an element has the rng namespace and the provided localName.
	 */
	static private boolean isRng(String localName, Element e) {
		return NC.rng.equals(e.getNamespaceURI())
				&& localName.equals(e.getLocalName());
	}

	/**
	 * Merge the information from <define/> elements into the <define/>,
	 * <element/> or <attribute/> element from which they are referred. This
	 * function goes into each referred <define/> element recursively.
	 * 
	 * @param info
	 *            The object into which the information will be aggregated
	 * @param ref
	 *            The name of the <define/> element that will be merged
	 * @param defines
	 *            The map with all <define/> elements.
	 */
	static private void addDefine(InfoBase info, String ref,
			HashMap<String, DefineInfo> defines) {
		DefineInfo define = defines.get(ref);
		if (define != null) {
			info.text = info.text || define.text;
			info.value.addAll(define.value);
			info.data.addAll(define.data);
			if (info instanceof ElementInfo) {
				ElementInfo ei = (ElementInfo) info;
				for (ElementInfo c : define.childElements) {
					if (!ei.childElements.contains(c)) {
						ei.childElements.add(c);
					}
				}
				for (AttributeInfo c : define.attributes) {
					if (!ei.attributes.contains(c)) {
						ei.attributes.add(c);
					}
				}
			}
			for (String d : define.refs) {
				addDefine(info, d, defines);
			}
		}
	}

	/**
	 * Parse a QName string (ns:localName) into a QName object.
	 * 
	 * @param nsmap
	 *            A map with prefix to namespace mappings
	 * @param qname
	 * @return
	 */
	static private QName parseQName(HashMap<String, String> nsmap, String qname) {
		String uri, localName;
		int p = qname.indexOf(":");
		if (p == -1) {
			localName = qname;
			uri = nsmap.get("");
		} else {
			String prefix = qname.substring(0, p);
			localName = qname.substring(p + 1);
			uri = nsmap.get(prefix);
			if (uri == null) {
				throw new RuntimeException("No uri for prefix " + prefix);
			}
		}
		return QName.qname(uri, localName);
	}

	/**
	 * Parse the <data/> element.
	 */
	static private Data parseData(Element e) {
		Data data = new Data(e.getAttribute("type"));
		Element c = getFirstElementChild(e);
		while (c != null) {
			if (isRng("param", c)) {
				data.params.put(c.getAttribute("name"), c.getTextContent());
			}
			c = getNextElementSibling(c);
		}
		return data;
	}

	/**
	 * Recursively collect information from the RNG grammar.
	 * 
	 * @param e
	 *            The current element
	 * @param defs
	 *            A map with all <define/> elements by 'name' attribute
	 * @param current
	 *            The current object into which information is collected
	 * @param elements
	 *            Collected information from <element/> elements
	 * @param attributes
	 *            Collected information from <attribute/> elements
	 * @param defines
	 *            Collected information from <define/> elements
	 * @param nsmap
	 */
	static private void handleChildElements(Element e,
			HashMap<String, Element> defs, @Nullable InfoBase current,
			HashMap<QName, LinkedList<ElementInfo>> elements,
			HashMap<QName, LinkedList<AttributeInfo>> attributes,
			HashMap<String, DefineInfo> defines, HashMap<String, String> nsmap) {
		Element c = getFirstElementChild(e);
		while (c != null) {
			if (isRng("ref", c)) {
				final String name = c.getAttribute("name");
				if (current != null) {
					current.refs.add(name);
				}
				final Element def = defs.remove(name);
				if (def != null) {
					DefineInfo d = new DefineInfo();
					defines.put(name, d);
					handleChildElements(def, defs, d, elements, attributes,
							defines, nsmap);
				}
			} else if (isRng("element", c)) {
				final Set<String> names = getNames(c);
				final ElementInfo info = new ElementInfo(names);
				for (String name : names) {
					LinkedList<ElementInfo> l = elements.get(name);
					if (l == null) {
						l = new LinkedList<ElementInfo>();
						elements.put(parseQName(nsmap, name), l);
					}
					l.add(info);
				}
				if (current != null) {
					((DefineInfo) current).childElements.add(info);
				}
				handleChildElements(c, defs, info, elements, attributes,
						defines, nsmap);
			} else if (isRng("attribute", c)) {
				final Set<String> names = getNames(c);
				final AttributeInfo info = new AttributeInfo(names);
				for (String name : names) {
					LinkedList<AttributeInfo> l = attributes.get(name);
					if (l == null) {
						l = new LinkedList<AttributeInfo>();
						attributes.put(parseQName(nsmap, name), l);
					}
					l.add(info);
				}
				if (current != null) {
					((DefineInfo) current).attributes.add(info);
				}
				handleChildElements(c, defs, info, elements, attributes,
						defines, nsmap);
			} else if (isRng("text", c) && current != null) {
				current.text = true;
			} else if (isRng("mixed", c) && current != null) {
				current.text = true;
				handleChildElements(c, defs, current, elements, attributes,
						defines, nsmap);
			} else if (isRng("data", c) && current != null) {
				current.data.add(parseData(c));
			} else if (isRng("value", c) && current != null) {
				current.value.add(c.getNodeValue());
			} else if (isRng("define", c)) {
				throw new RuntimeException();
			} else {
				handleChildElements(c, defs, current, elements, attributes,
						defines, nsmap);
			}
			c = getNextElementSibling(c);
		}
	}

	/**
	 * Collect the namespace definitions that are defined on this element. This
	 * function is not recursive. The key in the returned map is the prefix, the
	 * value is the namespace.
	 */
	static private HashMap<String, String> getNamespaces(Element e) {
		final HashMap<String, String> ns = new HashMap<String, String>();
		ns.put("xml", NC.xml);
		ns.put("xmlns", NC.xmlns);
		final NamedNodeMap atts = e.getAttributes();
		for (int i = 0; i < atts.getLength(); ++i) {
			final Attr a = (Attr) atts.item(i);
			if (NC.xmlns.equals(a.getNamespaceURI())) {
				ns.put(a.getLocalName(), a.getValue());
			} else if (a.getNamespaceURI() == null
					&& a.getLocalName().equals("ns")) {
				ns.put("", a.getValue());
			}
		}
		return ns;
	}

	/**
	 * Obtain the names of the <define/> elements that are redefined before
	 * being included.
	 *
	 * @param includeElement
	 * @return
	 */
	static private Set<String> getOverriddenDefines(Element includeElement) {
		Set<String> names = new HashSet<String>();
		Element e = getFirstElementChild(includeElement);
		while (e != null) {
			if (isRng("define", e)) {
				names.add(e.getAttribute("name"));
			}
			e = getNextElementSibling(e);
		}
		return names;
	}

	/**
	 * Copy the namespace declaration from the document element of a to that of
	 * b
	 *
	 * @param a
	 * @param b
	 */
	static void copyNamespaceDeclaration(Document a, Document b) {
		final Element ae = a.getDocumentElement();
		final Element be = b.getDocumentElement();
		final NamedNodeMap atts = ae.getAttributes();
		for (int i = 0; i < atts.getLength(); ++i) {
			final Attr att = (Attr) atts.item(i);
			if (NC.xmlns.equals(att.getNamespaceURI())) {
				be.setAttributeNS(NC.xmlns, att.getName(), att.getValue());
			}
		}
	}

	/**
	 * Find all <include/> elements and handle them. The implementation is quite
	 * minimal, just to get ODF RNG files to work. The grammar is modified in
	 * place
	 *
	 * @param grammar
	 *            The grammer to be modified
	 * @param xml
	 *            The xml loader
	 * @throws IOException
	 */
	static void handleIncludes(Document grammar, XML xml) throws IOException {
		final File dir = new File(grammar.getDocumentURI()).getParentFile();
		NodeList list = grammar.getElementsByTagNameNS(NC.rng, "include");
		for (int i = 0; i < list.getLength(); ++i) {
			final Element includeElement = (Element) list.item(i);
			final Node parent = includeElement.getParentNode();
			final Set<String> names = getOverriddenDefines(includeElement);
			Node n = includeElement.getFirstChild();
			while (n != null) {
				Node c = n.getNextSibling();
				parent.insertBefore(n, includeElement);
				n = c;
			}
			String href = includeElement.getAttribute("href");
			File includeFile = new File(dir, href);
			Document doc = xml.parse(new FileInputStream(includeFile), null, null);
			n = doc.getDocumentElement().getFirstChild();
			while (n != null) {
				Node c = n.getNextSibling();
				if (n.getNodeType() == Node.ELEMENT_NODE) {
					Element e = (Element) n;
					if (!isRng("define", e)
							|| !names.contains(e.getAttribute("name"))) {
						parent.insertBefore(grammar.importNode(n, true),
								includeElement);
					}
				} else {
					parent.insertBefore(grammar.importNode(n, true),
							includeElement);
				}
				n = c;
			}
			copyNamespaceDeclaration(doc, grammar);
			includeElement.getParentNode().removeChild(includeElement);
		}
	}

	/**
	 * Parse the grammar and place all obtained information in the two provided
	 * maps.
	 *
	 * @throws IOException
	 */
	static void parse(Document grammar,
			HashMap<QName, LinkedList<ElementInfo>> elements,
			HashMap<QName, LinkedList<AttributeInfo>> attributes) {

		// handle the combine attributes in <define/>
		RngNormalizer.normalize(grammar);

		HashMap<String, String> ns = getNamespaces(grammar.getDocumentElement());
		Element start = XPath
				.elementIterator(grammar, "/rng:grammar/rng:start").get(0);
		HashMap<String, Element> defs = getDefinitions(grammar);
		HashMap<String, DefineInfo> defines = new HashMap<String, DefineInfo>();
		handleChildElements(start, defs, null, elements, attributes, defines,
				ns);
		defs.clear();
		for (LinkedList<ElementInfo> l : elements.values()) {
			for (ElementInfo ei : l) {
				for (String ref : ei.refs) {
					addDefine(ei, ref, defines);
				}
			}
		}
		for (LinkedList<AttributeInfo> l : attributes.values()) {
			for (AttributeInfo ai : l) {
				for (String ref : ai.refs) {
					addDefine(ai, ref, defines);
				}
			}
		}
		defines.clear();
		for (LinkedList<ElementInfo> l : elements.values()) {
			for (ElementInfo e1 : l) {
				for (ElementInfo e2 : e1.childElements) {
					e2.parentElements.add(e1);
				}
			}
			for (ElementInfo e : l) {
				for (AttributeInfo a : e.attributes) {
					a.parentElements.add(e);
				}
			}
		}
	}
}

class Data {
	final String type;
	final HashMap<String, String> params = new HashMap<String, String>();

	Data(String type) {
		this.type = type;
	}
}

class InfoBase {
	final TreeSet<String> refs = new TreeSet<String>();
	boolean text = false;
	LinkedList<Data> data = new LinkedList<Data>();
	LinkedList<String> value = new LinkedList<String>();
}

class DefineInfo extends InfoBase {
	final LinkedList<ElementInfo> childElements = new LinkedList<ElementInfo>();
	final LinkedList<AttributeInfo> attributes = new LinkedList<AttributeInfo>();
}

class ElementInfo extends DefineInfo {
	final Set<String> names;
	LinkedList<ElementInfo> parentElements = new LinkedList<ElementInfo>();

	ElementInfo(Set<String> names) {
		this.names = names;
	}
}

class AttributeInfo extends InfoBase {
	final Set<String> names;
	LinkedList<ElementInfo> parentElements = new LinkedList<ElementInfo>();

	AttributeInfo(Set<String> names) {
		this.names = names;
	}
}