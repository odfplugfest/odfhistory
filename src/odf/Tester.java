package odf;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;

import odf.OdfInfo.FileType;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class Tester {
	static boolean runTests(Path dir, OdfNormalizer odfNormalizer)
			throws IOException, XMLStreamException, SAXException {

		boolean allPass = true;
		try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(
				dir, "*")) {
			for (Path path : directoryStream) {
				System.out.println(path);
				if (path.toString().endsWith("before.xml")) {
					try {
						allPass = allPass && runTest(path, odfNormalizer);
					} catch (TestFailException e) {
						allPass = false;
						System.out.println(e);
					}
				} else if (Files.isDirectory(path)) {
					allPass = allPass && runTests(path, odfNormalizer);
				}
			}
		}
		return allPass;
	}

	static private boolean runTest(Path beforePath, OdfNormalizer odfNormalizer)
			throws TestFailException, XMLStreamException, SAXException {
		Document doc;
		FileType type = beforePath.toString().contains("/manifest/") ? OdfInfo.FileType.ODFMANIFEST
				: OdfInfo.FileType.ODF;
		OdfInfo odfInfo = new OdfInfo(type);
		StyleNames styleNames = new StyleNames();
		try {
			doc = odfNormalizer.xml.parse(
					new FileInputStream(beforePath.toFile()), odfInfo,
					styleNames);
		} catch (IOException e) {
			throw new TestFailException("Could not read test input.", e);
		}
		byte result[];
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			odfNormalizer.normalize(doc, styleNames);
			OdfHistory.pretty(doc, out, odfInfo.getRngInfo(odfNormalizer.xml)
					.getElementsWithNoText());
			out.close();
			result = out.toByteArray();
		} catch (NormalizeException | TransformerException | IOException e) {
			e.printStackTrace();
			throw new TestFailException("Error while normalizing document.", e);
		}

		String number = beforePath.getFileName().toString().substring(0, 3);
		Path afterPath = beforePath.getParent().resolve(number + "after.xml");

		byte after[];
		try {
			after = Files.readAllBytes(afterPath);
		} catch (IOException e) {
			throw new TestFailException("Could not read test result file.", e);
		}

		if (!Arrays.equals(result, after)) {
			System.err.println(result.length + " " + after.length);
			throw new TestFailException("Test " + number + " failed. Result:\n"
					+ new String(result) + "\nReference:\n" + new String(after));
		}

		return true;
	}
}

class TestFailException extends Exception {

	private static final long serialVersionUID = 8266924274445959863L;

	TestFailException(String msg) {
		super(msg);
	}

	TestFailException(String msg, Exception cause) {
		super(msg, cause);
	}
}