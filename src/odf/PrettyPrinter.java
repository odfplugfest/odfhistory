package odf;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Stack;
import java.util.TreeMap;

import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.eclipse.jdt.annotation.Nullable;

class PrettyPrinter implements XMLStreamWriter {
	final private XMLStreamWriter out;
	final private HashSet<QName> elementsWithNoText;
	final private LinkedList<String> initialComments = new LinkedList<String>();
	private boolean started = false;

	PrettyPrinter(XMLStreamWriter out, HashSet<QName> elementsWithNoText) {
		this.out = out;
		this.elementsWithNoText = elementsWithNoText;
	}

	@Override
	public void close() throws XMLStreamException {
		out.close();
	}

	@Override
	public void flush() throws XMLStreamException {
		out.flush();
	}

	@Override
	public NamespaceContext getNamespaceContext() {
		return out.getNamespaceContext();
	}

	@Override
	public String getPrefix(@Nullable String uri) throws XMLStreamException {
		return out.getPrefix(uri);
	}

	@Override
	public Object getProperty(@Nullable String name)
			throws IllegalArgumentException {
		return out.getProperty(name);
	}

	@Override
	public void setDefaultNamespace(@Nullable String uri)
			throws XMLStreamException {
		out.setDefaultNamespace(uri);
	}

	@Override
	public void setNamespaceContext(@Nullable NamespaceContext context)
			throws XMLStreamException {
		out.setNamespaceContext(context);
	}

	@Override
	public void setPrefix(@Nullable String prefix, @Nullable String uri)
			throws XMLStreamException {
		out.setPrefix(prefix, uri);
	}

	@Override
	public void writeAttribute(@Nullable String localName,
			@Nullable String value) throws XMLStreamException {
		attributes.add(new Attribute(null, null, localName, value));
	}

	@Override
	public void writeAttribute(@Nullable String namespaceURI,
			@Nullable String localName, @Nullable String value)
			throws XMLStreamException {
		attributes.add(new Attribute(null, namespaceURI, localName, value));
	}

	@Override
	public void writeAttribute(@Nullable String prefix,
			@Nullable String namespaceURI, @Nullable String localName,
			@Nullable String value) throws XMLStreamException {
		attributes.add(new Attribute(prefix, namespaceURI, localName, value));
	}

	@Override
	public void writeCData(@Nullable String data) throws XMLStreamException {
		startElement();
		out.writeCData(data);
	}

	@Override
	public void writeCharacters(@Nullable String text)
			throws XMLStreamException {
		startElement();
		out.writeCharacters(text);
	}

	@Override
	public void writeCharacters(@Nullable char[] text, int start, int len)
			throws XMLStreamException {
		startElement();
		out.writeCharacters(text, start, len);
	}

	@Override
	public void writeComment(@Nullable String data) throws XMLStreamException {
		if (!started) {
			initialComments.add(data);
			return;
		}
		startElement();
		if (stack.size() == 0 || stack.peek()) {
			indent();
		}
		out.writeComment(data);
	}

	@Override
	public void writeDTD(@Nullable String dtd) throws XMLStreamException {
		startElement();
		out.writeDTD(dtd);
	}

	@Override
	public void writeDefaultNamespace(@Nullable String namespaceURI)
			throws XMLStreamException {
		// out.writeDefaultNamespace(namespaceURI);
	}

	@Override
	public void writeEmptyElement(@Nullable String localName)
			throws XMLStreamException {
		writeStartElement(localName);
		writeEndElement();
	}

	@Override
	public void writeEmptyElement(@Nullable String namespaceURI,
			@Nullable String localName) throws XMLStreamException {
		writeStartElement(namespaceURI, localName);
		writeEndElement();
	}

	@Override
	public void writeEmptyElement(@Nullable String prefix,
			@Nullable String localName, @Nullable String namespaceURI)
			throws XMLStreamException {
		writeStartElement(prefix, localName, namespaceURI);
		writeEndElement();
	}

	@Override
	public void writeEndDocument() throws XMLStreamException {
		out.writeEndDocument();
		out.writeCharacters("\n");
	}

	@Override
	public void writeEndElement() throws XMLStreamException {
		endElement();
	}

	@Override
	public void writeEntityRef(@Nullable String name) throws XMLStreamException {
		out.writeEntityRef(name);
	}

	@Override
	public void writeNamespace(@Nullable String prefix,
			@Nullable String namespaceURI) throws XMLStreamException {
		namespaces.put(prefix, namespaceURI);
	}

	@Override
	public void writeProcessingInstruction(@Nullable String target)
			throws XMLStreamException {
		startElement();
		out.writeProcessingInstruction(target);
	}

	@Override
	public void writeProcessingInstruction(@Nullable String target,
			@Nullable String data) throws XMLStreamException {
		startElement();
		out.writeProcessingInstruction(target, data);
	}

	private void writeInitialComments() throws XMLStreamException {
		started = true;
		for (String comment : initialComments) {
			writeComment(comment);
		}
	}

	@Override
	public void writeStartDocument() throws XMLStreamException {
		out.writeStartDocument();
		writeInitialComments();
	}

	@Override
	public void writeStartDocument(@Nullable String version)
			throws XMLStreamException {
		out.writeStartDocument("UTF-8", version);
		writeInitialComments();
	}

	@Override
	public void writeStartDocument(@Nullable String encoding,
			@Nullable String version) throws XMLStreamException {
		out.writeStartDocument(encoding, version);
		writeInitialComments();
	}

	static QName parseQName(@Nullable String qname, NamespaceContext nc) {
		if (qname == null) {
			throw new NullPointerException();
		}
		String uri = null, localName;
		int p = qname.indexOf(":");
		if (p == -1) {
			localName = qname;
		} else {
			String prefix = qname.substring(0, p);
			localName = qname.substring(p + 1);
			uri = nc.getNamespaceURI(prefix);
			if (uri == null) {
				throw new RuntimeException("No uri for prefix " + prefix);
			}
		}
		return QName.qname(uri, localName);
	}

	@Override
	public void writeStartElement(@Nullable String localName)
			throws XMLStreamException {
		if (localName == null) {
			throw new NullPointerException();
		}
		startElement();
		elementStarted = false;
		this.prefix = null;
		this.namespaceURI = null;
		this.localName = localName;
	}

	@Override
	public void writeStartElement(@Nullable String namespaceURI,
			@Nullable String localName) throws XMLStreamException {
		if (localName == null) {
			throw new NullPointerException();
		}
		startElement();
		elementStarted = false;
		this.prefix = null;
		this.namespaceURI = namespaceURI;
		this.localName = localName;
	}

	@Override
	public void writeStartElement(@Nullable String prefix,
			@Nullable String localName, @Nullable String namespaceURI)
			throws XMLStreamException {
		if (localName == null) {
			throw new NullPointerException();
		}
		startElement();
		elementStarted = false;
		this.prefix = prefix;
		this.namespaceURI = namespaceURI;
		this.localName = localName;
	}

	private boolean shouldIndent() {
		QName qname = parseQName(localName, getNamespaceContext());
		return elementsWithNoText.contains(qname);
	}

	private TreeMap<String, String> namespaces = new TreeMap<String, String>();
	private ArrayList<Attribute> attributes = new ArrayList<Attribute>();

	private boolean elementStarted = true;
	@Nullable
	private String prefix;
	@Nullable
	private String namespaceURI;
	private String localName = "";

	private int level = 0;

	private Stack<Boolean> stack = new Stack<Boolean>();

	private void startElement() throws XMLStreamException {
		if (elementStarted) {
			return;
		}
		elementStarted = true;
		boolean indent = shouldIndent();
		if (stack.size() == 0 || stack.peek()) {
			indent();
		}
		if (indent) {
			level += 1;
		}
		stack.add(indent);
		if (prefix != null) {
			out.writeStartElement(prefix, localName, namespaceURI);
		} else if (namespaceURI != null) {
			out.writeStartElement(namespaceURI, localName);
		} else {
			out.writeStartElement(localName);
		}
		writeNamespaces();
		writeAttributes();
	}

	private void writeNamespaces() throws XMLStreamException {
		for (Entry<String, String> e : namespaces.entrySet()) {
			out.writeNamespace(e.getKey(), e.getValue());
		}
		namespaces.clear();
	}

	private void writeAttributes() throws XMLStreamException {
		for (Attribute a : attributes) {
			if (a.prefix != null) {
				out.writeAttribute(a.prefix, a.namespaceURI, a.localName,
						a.value);
			} else if (a.namespaceURI != null) {
				out.writeAttribute(a.namespaceURI, a.localName, a.value);
			} else {
				out.writeAttribute(a.localName, a.value);
			}
		}
		attributes.clear();
	}

	private void endElement() throws XMLStreamException {
		if (!elementStarted) {
			elementStarted = true;
			if (stack.size() == 0 || stack.peek()) {
				indent();
			}
			if (prefix != null) {
				out.writeEmptyElement(prefix, localName, namespaceURI);
			} else if (namespaceURI != null) {
				out.writeEmptyElement(namespaceURI, localName);
			} else {
				out.writeEmptyElement(localName);
			}
			writeNamespaces();
			writeAttributes();
		} else {
			if (stack.pop()) {
				level -= 1;
				indent();
			}
			out.writeEndElement();
		}
	}

	private void indent() throws XMLStreamException {
		out.writeCharacters("\n");
		for (int i = 0; i < level; ++i) {
			out.writeCharacters("  ");
		}
	}
}

class Attribute {
	final @Nullable String prefix;
	final @Nullable String namespaceURI;
	final @Nullable String localName;
	final @Nullable String value;

	Attribute(@Nullable String prefix, @Nullable String namespaceURI,
			@Nullable String localName, @Nullable String value) {
		this.prefix = prefix;
		this.namespaceURI = namespaceURI;
		this.localName = localName;
		this.value = value;
	}
}
