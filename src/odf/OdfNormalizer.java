package odf;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jdt.annotation.Nullable;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;

public class OdfNormalizer {

	final XML xml = new XML();
	final OdfInfo odfManifestInfo;
	final OdfInfo odfInfo;
	final OdfInfo rngInfo;

	OdfNormalizer() {
		odfManifestInfo = new OdfInfo(OdfInfo.FileType.ODFMANIFEST);
		odfInfo = new OdfInfo(OdfInfo.FileType.ODF);
		rngInfo = new OdfInfo(OdfInfo.FileType.RNG);
	}

	static public LinkedList<String> cleanManifest(Document manifest) {
		Set<String> done = new HashSet<String>();
		LinkedList<String> entries = new LinkedList<String>();
		for (Attr a : XPath.attrIterator(manifest,
				"/manifest:manifest/manifest:file-entry/@manifest:full-path")) {
			String path = a.getNodeValue();
			String mediaType = a.getOwnerElement().getAttributeNS(NC.manifest,
					"media-type");
			Element e = a.getOwnerElement();
			if (path.endsWith("/")
					&& mediaType
							.startsWith("application/vnd.oasis.opendocument.")) {
				continue;
			} else if (done.contains(path)
					|| path.endsWith("/")
					|| path.equals("Configurations2/accelerator/current.xml")
					|| path.endsWith("/Configurations2/accelerator/current.xml")
					|| path.equals("layout-cache") || path.equals("mimetype")
					|| path.equals("META-INF/manifest.xml")
					|| path.equals("settings.xml")
					|| path.endsWith("/settings.xml")) {
				e.getParentNode().removeChild(e);
				continue;
			} else {
				done.add(path);
				entries.add(path);
			}
			if (path.endsWith(".png")) {
				e.getAttributeNodeNS(NC.manifest, "media-type").setNodeValue(
						"image/png");
			}
		}
		sortManifestEntries(manifest);
		synchronizeVersionAttribute(manifest);
		NamespaceCleaner.clean(manifest);
		return entries;
	}

	static private void synchronizeVersionAttribute(Document manifest) {
		Element root = manifest.getDocumentElement();
		Element entry = (Element) root.getFirstChild();
		Attr version = entry.getAttributeNodeNS(NC.manifest, "version");
		if (version != null) {
			root.setAttributeNS(NC.manifest, "manifest:version",
					version.getNodeValue());
		} else {
			version = root.getAttributeNodeNS(NC.manifest, "version");
			if (version != null) {
				entry.setAttributeNS(NC.manifest, "manifest:version",
						version.getNodeValue());
			}
		}
	}

	static public void sortManifestEntries(Document manifest) {
		LinkedList<Element> entries = new LinkedList<Element>();
		for (Element e : XPath.elementIterator(manifest,
				"/manifest:manifest/manifest:file-entry")) {
			entries.add(e);
		}
		Collections.sort(entries, manifestSorter);
		for (Element e : entries) {
			e.getParentNode().appendChild(e);
		}
	}

	static public void sortAutomaticStyles(Document doc) {
		LinkedList<Element> entries = new LinkedList<Element>();
		for (Element e : XPath.elementIterator(doc,
				"/*/office:automatic-styles/*")) {
			entries.add(e);
		}
		Collections.sort(entries, shallowElementSorter);
		for (Element e : entries) {
			e.getParentNode().appendChild(e);
		}
	}

	static public void cleanMeta(Document meta) {
		LinkedList<Element> entries = new LinkedList<Element>();
		for (Element e : XPath.elementIterator(meta, "/*/office:meta/*")) {
			entries.add(e);
		}
		Collections.sort(entries, shallowElementSorter);
		for (Element e : entries) {
			e.getParentNode().appendChild(e);
		}
		NamespaceCleaner.clean(meta);
	}

	static final Comparator<Element> manifestSorter = new Comparator<Element>() {

		@Override
		public int compare(@Nullable Element e1, @Nullable Element e2) {
			if (e1 == e2) {
				return 0;
			}
			if (e1 == null) {
				return -1;
			}
			if (e2 == null) {
				return 1;
			}
			String s1 = e1.getAttributeNS(NC.manifest, "full-path");
			String s2 = e2.getAttributeNS(NC.manifest, "full-path");
			if ("/".equals(s1)) {
				return -1;
			}
			if ("/".equals(s2)) {
				return 1;
			}
			return s1.compareTo(s2);
		}
	};

	static final Comparator<Element> shallowElementSorter = new Comparator<Element>() {

		private int compare(@Nullable String a, @Nullable String b) {
			if (a == null) {
				return (b == null) ? 0 : 1;
			}
			if (b == null) {
				return -1;
			}
			return a.compareTo(b);
		}

		private void addAttributes(TreeSet<QName> set, Element e) {
			NamedNodeMap atts = e.getAttributes();
			int l = atts.getLength();
			for (int i = 0; i < l; ++i) {
				Node a = atts.item(i);
				set.add(QName.qname(a.getNamespaceURI(), a.getLocalName()));
			}
		}

		private @Nullable String attribute(Element e, QName qname) {
			if (e.hasAttributeNS(qname.namespaceURI, qname.localName)) {
				return e.getAttributeNS(qname.namespaceURI, qname.localName);
			}
			return null;
		}

		@Override
		public int compare(@Nullable Element e1, @Nullable Element e2) {
			if (e1 == e2) {
				return 0;
			}
			if (e1 == null) {
				return -1;
			}
			if (e2 == null) {
				return 1;
			}
			int c = compare(e1.getNamespaceURI(), e2.getNamespaceURI());
			if (c != 0) {
				return c;
			}
			c = compare(e1.getLocalName(), e2.getLocalName());
			if (c != 0) {
				return c;
			}
			TreeSet<QName> attributesQNames = new TreeSet<QName>();
			addAttributes(attributesQNames, e1);
			addAttributes(attributesQNames, e2);
			for (QName q : attributesQNames) {
				c = compare(attribute(e1, q), attribute(e2, q));
				if (c != 0) {
					return c;
				}
			}
			c = compare(e1.getTextContent(), e2.getTextContent());
			return c;
		}
	};

	static final Comparator<Node> referenceMarkSorter = new Comparator<Node>() {

		@Override
		public int compare(@Nullable Node n1, @Nullable Node n2) {
			if (n1 == n2) {
				return 0;
			}
			if (n1 == null) {
				return -1;
			}
			if (n2 == null) {
				return 1;
			}
			int t1 = n1.getNodeType();
			int c = t1 - n2.getNodeType();
			if (c != 0) {
				return c;
			}
			if (t1 == Node.TEXT_NODE) {
				return n1.getNodeValue().compareTo(n2.getNodeValue());
			}
			final Element e1 = (Element) n1;
			final Element e2 = (Element) n2;
			c = markList.indexOf(e1.getLocalName())
					- markList.indexOf(e2.getLocalName());
			if (c != 0) {
				return c;
			}
			String s1 = e1.getAttributeNS(NC.text, "name");
			String s2 = e2.getAttributeNS(NC.text, "name");
			return s1.compareTo(s2);
		}
	};

	static private void removeIfEqual(Element src, String srcns,
			String srcname, Element ref, String refns, String refname) {
		Attr a = ref.getAttributeNodeNS(refns, refname);
		if (a != null
				&& a.getNodeValue().equals(src.getAttributeNS(srcns, srcname))) {
			ref.removeAttributeNode(a);
		}
	}

	static private void removeUnneededFontAttributes(Document doc) {
		Map<String, Element> fonts = new HashMap<String, Element>();
		for (Element f : XPath.elementIterator(doc,
				"/*/office:font-face-decls/style:font-face")) {
			fonts.put(f.getAttributeNS(NC.style, "name"), f);
		}
		XPathResult<Attr> names = XPath.attrIterator(doc, "//@style:font-name");
		for (Attr name : names) {
			Element font = fonts.get(name.getNodeValue());
			Element style = name.getOwnerElement();
			if (font == null) {
				continue;
			}
			removeIfEqual(font, NC.svg, "font-family", style, NC.fo,
					"font-family");
			removeIfEqual(font, NC.svg, "font-style", style, NC.style,
					"font-style-name");
			removeIfEqual(font, NC.style, "font-family-generic", style,
					NC.style, "font-family-generic");
			removeIfEqual(font, NC.style, "font-pitch", style, NC.style,
					"font-pitch");
			removeIfEqual(font, NC.style, "font-charset", style, NC.style,
					"font-charset");
		}
		names = XPath.attrIterator(doc, "//@style:font-name-asian");
		for (Attr name : names) {
			Element font = fonts.get(name.getNodeValue());
			Element style = name.getOwnerElement();
			if (font == null) {
				continue;
			}
			removeIfEqual(font, NC.svg, "font-family", style, NC.style,
					"font-family-asian");
			removeIfEqual(font, NC.svg, "font-style", style, NC.style,
					"font-style-name-asian");
			removeIfEqual(font, NC.style, "font-family-generic", style,
					NC.style, "font-family-generic-asian");
			removeIfEqual(font, NC.style, "font-pitch", style, NC.style,
					"font-pitch-asian");
			removeIfEqual(font, NC.style, "font-charset", style, NC.style,
					"font-charset-asian");
		}
		names = XPath.attrIterator(doc, "//@style:font-name-complex");
		for (Attr name : names) {
			Element font = fonts.get(name.getNodeValue());
			Element style = name.getOwnerElement();
			if (font == null) {
				continue;
			}
			removeIfEqual(font, NC.svg, "font-family", style, NC.style,
					"font-family-complex");
			removeIfEqual(font, NC.svg, "font-style", style, NC.style,
					"font-style-name-complex");
			removeIfEqual(font, NC.style, "font-family-generic", style,
					NC.style, "font-family-generic-complex");
			removeIfEqual(font, NC.style, "font-pitch", style, NC.style,
					"font-pitch-complex");
			removeIfEqual(font, NC.style, "font-charset", style, NC.style,
					"font-charset-complex");
		}
	}

	static private void removeUnusedListIdsAndReferences(Document doc) {
		XPathResult<Attr> ids = XPath.attrIterator(doc, "//text:list/@xml:id");
		XPathResult<Attr> refs = XPath.attrIterator(doc,
				"//text:list/@text:continue-list");

		HashSet<String> usedIds = new HashSet<String>();
		HashSet<String> idss = new HashSet<String>();

		for (Attr a : ids) {
			idss.add(a.getNodeValue());
		}
		for (Attr ref : refs) {
			final String r = ref.getNodeValue();
			if (idss.contains(r)) {
				usedIds.add(r);
			} else {
				ref.getOwnerElement().removeAttributeNode(ref);
			}
		}
		for (Attr a : ids) {
			if (!usedIds.contains(a.getNodeValue())) {
				a.getOwnerElement().removeAttributeNode(a);
			}
		}
	}

	public void normalize(Document doc, StyleNames styleNames)
			throws NormalizeException {
		final Element documentElement = doc.getDocumentElement();
		if (documentElement.getNamespaceURI().equals(NC.manifest)) {
			cleanManifest(doc);
			return;
		}
		if (documentElement.getNamespaceURI().equals(NC.office)
				&& documentElement.getLocalName().equals("document-meta")) {
			cleanMeta(doc);
			return;
		}

		// there is no point in z-index for objects with char anchor
		remove(doc, "//@draw:z-index[../@text:anchor-type='as-char']");
		remove(doc, "//text:soft-page-break");
		remove(doc, "/*/office:settings");
		remove(doc, "/*/office:scripts");

		// remove layout specific values
		remove(doc, "//text:page-number/text()");
		remove(doc, "//text:page-count/text()");

		// remove empty elements
		remove(doc, "//table:named-expressions[not(*) and not(@*)]");
		remove(doc, "//style:background-image[not(*) and not(@*)]");

		// remove this for odf spec, not for general use
		// best is to find a way to make stable identifiers
		remove(doc, "//text:alphabetical-index-mark-start");
		remove(doc, "//text:alphabetical-index-mark-end");
		remove(doc, "//text:toc-mark-start");
		remove(doc, "//text:toc-mark-end");
		remove(doc, "//@text:formula[starts-with(.,'ooow:')]");

		// remove continued list for which the first list item explicitly starts
		// at position 1
		remove(doc,
				"//text:list/@text:continue-list[parent::text:list/text:list-item[1][@text:start-value='1']]");
		removeUnusedListIdsAndReferences(doc);

		removeUnneededFontAttributes(doc);

		fixBibliographyStyles(doc, styleNames);
		addLinkStyle(doc, styleNames);
		setTextLineThroughType(doc);

		IdAndReferenceNormalizer n = new IdAndReferenceNormalizer(doc);
		// fonts
		n.addIdsAndReferences("F", "//style:font-face/@style:name",
				"//@style:font-name" + "|//@style:font-name-asian"
						+ "|//@style:font-name-complex", true);
		// family text
		n.addIdsAndReferences(
				"T",
				"/*/office:automatic-styles/style:style[@style:family='text']/@style:name",
				"//style:tab-stop/@style:leader-text-style"
						+ "|//style:drop-cap/@style:style-name"
						+ "|//text:notes-configuration/@text:citation-body-style-name"
						+ "|//text:notes-configuration/@text:citation-style-name"
						+ "|//text:a/@text:style-name"
						+ "|//text:alphabetical-index/@text:style-name"
						+ "|//text:linenumbering-configuration/@text:style-name"
						+ "|//text:list-level-style-number/@text:style-name"
						+ "|//text:ruby-text/@text:style-name"
						+ "|//text:span/@text:style-name"
						+ "|//text:a/@text:visited-style-name"
						+ "|//style:text-properties/@style:text-line-through-text-style"
						+ "|//text:alphabetical-index-source/@text:main-entry-style-name"
						+ "|//text:index-entry-bibliography/@text:style-name"
						+ "|//text:index-entry-chapter/@text:style-name"
						+ "|//text:index-entry-link-end/@text:style-name"
						+ "|//text:index-entry-link-start/@text:style-name"
						+ "|//text:index-entry-page-number/@text:style-name"
						+ "|//text:index-entry-span/@text:style-name"
						+ "|//text:index-entry-tab-stop/@text:style-name"
						+ "|//text:index-entry-text/@text:style-name"
						+ "|//text:index-title-template/@text:style-name"
						+ "|//text:list-level-style-bullet/@text:style-name"
						+ "|//text:outline-level-style/@text:style-name", true);
		// family paragraph
		n.addIdsAndReferences(
				"P",
				"/*/office:automatic-styles/style:style[@style:family='paragraph']/@style:name",
				"//draw:caption/@draw:text-style-name"
						+ "|//draw:circle/@draw:text-style-name"
						+ "|//draw:connector/@draw:text-style-name"
						+ "|//draw:control/@draw:text-style-name"
						+ "|//draw:custom-shape/@draw:text-style-name"
						+ "|//draw:ellipse/@draw:text-style-name"
						+ "|//draw:frame/@draw:text-style-name"
						+ "|//draw:line/@draw:text-style-name"
						+ "|//draw:measure/@draw:text-style-name"
						+ "|//draw:path/@draw:text-style-name"
						+ "|//draw:polygon/@draw:text-style-name"
						+ "|//draw:polyline/@draw:text-style-name"
						+ "|//draw:rect/@draw:text-style-name"
						+ "|//draw:regular-polygon/@draw:text-style-name"
						+ "|//office:annotation/@draw:text-style-name"
						+ "|//form:column/@form:text-style-name"
						+ "|//style:style/@style:next-style-name"
						+ "|//table:body/@table:paragraph-style-name"
						+ "|//table:even-columns/@table:paragraph-style-name"
						+ "|//table:even-rows/@table:paragraph-style-name"
						+ "|//table:first-column/@table:paragraph-style-name"
						+ "|//table:first-row/@table:paragraph-style-name"
						+ "|//table:last-column/@table:paragraph-style-name"
						+ "|//table:last-row/@table:paragraph-style-name"
						+ "|//table:odd-columns/@table:paragraph-style-name"
						+ "|//table:odd-rows/@table:paragraph-style-name"
						+ "|//text:notes-configuration/@text:default-style-name"
						+ "|//text:alphabetical-index-entry-template/@text:style-name"
						+ "|//text:bibliography-entry-template/@text:style-name"
						+ "|//text:h/@text:style-name"
						+ "|//text:illustration-index-entry-template/@text:style-name"
						+ "|//text:index-source-style/@text:style-name"
						+ "|//text:object-index-entry-template/@text:style-name"
						+ "|//text:p/@text:style-name"
						+ "|//text:table-index-entry-template/@text:style-name"
						+ "|//text:table-of-content-entry-template/@text:style-name"
						+ "|//text:table-index-entry-template/@text:style-name"
						+ "|//text:user-index-entry-template/@text:style-name"
						+ "|//style:page-layout-properties/@style:register-truth-ref-style-name",
				true);
		// family chart
		n.addIdsAndReferences(
				"C",
				"/*/office:automatic-styles/style:style[@style:family='chart']/@style:name",
				"//chart:axis/@chart:style-name"
						+ "|//chart:chart/@chart:style-name"
						+ "|//chart:data-label/@chart:style-name"
						+ "|//chart:data-point/@chart:style-name"
						+ "|//chart:equation/@chart:style-name"
						+ "|//chart:error-indicator/@chart:style-name"
						+ "|//chart:floor/@chart:style-name"
						+ "|//chart:footer/@chart:style-name"
						+ "|//chart:grid/@chart:style-name"
						+ "|//chart:legend/@chart:style-name"
						+ "|//chart:mean-value/@chart:style-name"
						+ "|//chart:plot-area/@chart:style-name"
						+ "|//chart:regression-curve/@chart:style-name"
						+ "|//chart:series/@chart:style-name"
						+ "|//chart:stock-gain-marker/@chart:style-name"
						+ "|//chart:stock-loss-marker/@chart:style-name"
						+ "|//chart:stock-range-line/@chart:style-name"
						+ "|//chart:subtitle/@chart:style-name"
						+ "|//chart:title/@chart:style-name"
						+ "|//chart:wall/@chart:style-name", true);
		// family section
		n.addIdsAndReferences(
				"S",
				"/*/office:automatic-styles/style:style[@style:family='section']/@style:name",
				"//text:alphabetical-index/@text:style-name"
						+ "|//text:bibliography/@text:style-name"
						+ "|//text:illustration-index/@text:style-name"
						+ "|//text:index-title/@text:style-name"
						+ "|//text:object-index/@text:style-name"
						+ "|//text:section/@text:style-name"
						+ "|//text:table-of-content/@text:style-name"
						+ "|//text:table-index/@text:style-name"
						+ "|//text:user-index/@text:style-name", true);
		// family ruby
		n.addIdsAndReferences(
				"R",
				"/*/office:automatic-styles/style:style[@style:family='ruby']/@style:name",
				"//text:ruby/@text:style-name", true);
		// family table
		n.addIdsAndReferences(
				"T",
				"/*/office:automatic-styles/style:style[@style:family='table']/@style:name",
				"//db:query/@db:style-name"
						+ "|//db:table-representation/@db:style-name"
						+ "|//table:background/@table:style-name"
						+ "|//table:table/@table:style-name", true);
		// family table-column
		n.addIdsAndReferences(
				"TC",
				"/*/office:automatic-styles/style:style[@style:family='table-column']/@style:name",
				"//db:column/@db:style-name"
						+ "|//table:table-column/@table:style-name", true);
		// family table-row
		n.addIdsAndReferences(
				"TR",
				"/*/office:automatic-styles/style:style[@style:family='table-row']/@style:name",
				"//db:query/@db:default-row-style-name"
						+ "|//db:table-representation/@db:default-row-style-name"
						+ "|//table:table-row/@table:style-name", true);
		// family table-cell
		n.addIdsAndReferences(
				"TC",
				"/*/office:automatic-styles/style:style[@style:family='table-cell']/@style:name",
				"//db:column/@db:default-cell-style-name"
						+ "|//table:table-column/@table:default-cell-style-name"
						+ "|//table:table-row/@table:default-cell-style-name"
						+ "|//table:body/@table:style-name"
						+ "|//table:covered-table-cell/@table:style-name"
						+ "|//table:even-columns/@table:style-name"
						+ "|//table:covered-table-cell/@table:style-name"
						+ "|//table:even-columns/@table:style-name"
						+ "|//table:even-rows/@table:style-name"
						+ "|//table:first-column/@table:style-name"
						+ "|//table:first-row/@table:style-name"
						+ "|//table:last-column/@table:style-name"
						+ "|//table:last-row/@table:style-name"
						+ "|//table:odd-columns/@table:style-name"
						+ "|//table:odd-rows/@table:style-name"
						+ "|//table:table-cell/@table:style-name", true);
		// family graphic
		n.addIdsAndReferences(
				"G",
				"/*/office:automatic-styles/style:style[@style:family='graphic']/@style:name",
				"//dr3d:cube/@draw:style-name"
						+ "|//dr3d:extrude/@draw:style-name"
						+ "|//dr3d:rotate/@draw:style-name"
						+ "|//dr3d:scene/@draw:style-name"
						+ "|//dr3d:sphere/@draw:style-name"
						+ "|//draw:caption/@draw:style-name"
						+ "|//draw:circle/@draw:style-name"
						+ "|//draw:connector/@draw:style-name"
						+ "|//draw:control/@draw:style-name"
						+ "|//draw:custom-shape/@draw:style-name"
						+ "|//draw:ellipse/@draw:style-name"
						+ "|//draw:frame/@draw:style-name"
						+ "|//draw:g/@draw:style-name"
						+ "|//draw:line/@draw:style-name"
						+ "|//draw:measure/@draw:style-name"
						+ "|//draw:page-thumbnail/@draw:style-name"
						+ "|//draw:path/@draw:style-name"
						+ "|//draw:polygon/@draw:style-name"
						+ "|//draw:polyline/@draw:style-name"
						+ "|//draw:rect/@draw:style-name"
						+ "|//draw:regular-polygon/@draw:style-name"
						+ "|//office:annotation/@draw:style-name", true);
		// family presentation
		n.addIdsAndReferences(
				"PR",
				"/*/office:automatic-styles/style:style[@style:family='presentation']/@style:name",
				"//dr3d:cube/@presentation:style-name"
						+ "|//dr3d:extrude/@presentation:style-name"
						+ "|//dr3d:rotate/@presentation:style-name"
						+ "|//dr3d:scene/@presentation:style-name"
						+ "|//dr3d:sphere/@presentation:style-name"
						+ "|//draw:caption/@presentation:style-name"
						+ "|//draw:circle/@presentation:style-name"
						+ "|//draw:connector/@presentation:style-name"
						+ "|//draw:control/@presentation:style-name"
						+ "|//draw:custom-shape/@presentation:style-name"
						+ "|//draw:ellipse/@presentation:style-name"
						+ "|//draw:frame/@presentation:style-name"
						+ "|//draw:g/@presentation:style-name"
						+ "|//draw:line/@presentation:style-name"
						+ "|//draw:measure/@presentation:style-name"
						+ "|//draw:page-thumbnail/@presentation:style-name"
						+ "|//draw:path/@presentation:style-name"
						+ "|//draw:polygon/@presentation:style-name"
						+ "|//draw:polyline/@presentation:style-name"
						+ "|//draw:rect/@presentation:style-name"
						+ "|//draw:regular-polygon/@presentation:style-name"
						+ "|//office:annotation/@presentation:style-name", true);
		// family drawing-page
		n.addIdsAndReferences(
				"D",
				"/*/office:automatic-styles/style:style[@style:family='drawing-page']/@style:name",
				"//draw:page/@draw:style-name"
						+ "|//presentation:notes/@draw:style-name"
						+ "|//style:handout-master/@draw:style-name"
						+ "|//style:master-page/@draw:style-name", true);
		// list styles
		n.addIdsAndReferences("L",
				"/*/office:automatic-styles/text:list-style/@style:name",
				"//text:list/@text:style-name"
						+ "|//text:numbered-paragraph/@text:style-name"
						+ "|//text:list-item/@text:style-override"
						+ "|//style:style/@style:list-style-name", true);

		n.addIdsAndReferences(
				"N",
				"/*/office:automatic-styles/number:boolean-style/@style:name"
						+ "|/*/office:automatic-styles/number:currency-style/@style:name"
						+ "|/*/office:automatic-styles/number:date-style/@style:name"
						+ "|/*/office:automatic-styles/number:number-style/@style:name"
						+ "|/*/office:automatic-styles/number:text-style/@style:name"
						+ "|/*/office:automatic-styles/number:time-style/@style:name",
				"//@style:data-style-name", true);

		n.addIdsAndReferences(
				"NP",
				"/*/office:automatic-styles/number:percentage-style/@style:name",
				"/*/*/style:style/@style:percentage-data-style-name", true);

		// n.addIdsAndReferences("//text:list/@xml:id",
		// "//text:list/@text:continue-list", false);

		n.normalize();

		sortAutomaticStyles(doc);
		fixBookmarkStart(doc);
		fixReferenceMarkOrder(doc, xml);
		mergeSpans(doc);
		NamespaceCleaner.clean(doc);
	}

	static private final String markArray[] = { "bookmark-end",
			"reference-mark-end", "bookmark", "reference-mark-start",
			"bookmark-start" };
	static private final List<String> markList = Arrays.asList(markArray);
	static private final HashSet<String> marks = new HashSet<String>();
	static {
		for (String m : markList) {
			marks.add(m);
		}
	}

	static private boolean isMark(Node n) {
		return n.getNodeType() == Node.ELEMENT_NODE
				&& NC.text.equals(n.getNamespaceURI())
				&& marks.contains(n.getLocalName());
	}

	final Matcher matcher = Pattern.compile("^_[0-9a-f][0-9a-f]__20_(.*)$")
			.matcher("");

	// workaround for bug in LO 5.1.2 where text:style-name="Strong_20_Emphasis"
	// is saved as text:style-name="_30__20_Strong_20_Emphasis" and new
	// attributes text:style-name="_30__20_" are introduces. The '_30_' can
	// vary.
	private void fixBibliographyStyles(Document doc, StyleNames styleNames) {
		XPathResult<Attr> atts = XPath.attrIterator(doc,
				"//text:index-entry-bibliography/@text:style-name");
		for (Attr att : atts) {
			matcher.reset(att.getNodeValue());
			if (matcher.find()) {
				String newName = matcher.group(1);
				if (newName.length() > 0
						&& styleNames.hasStyle(newName, "text")) {
					att.setNodeValue(newName);
				} else {
					att.getOwnerElement().removeAttributeNode(att);
				}
			}
		}
	}

	// workaround for LO behaviour where text:a without a style gets a default
	// style
	private void addLinkStyle(Document doc, StyleNames styleNames) {
		XPathResult<Element> es = XPath.elementIterator(doc, "//text:a");
		for (Element e : es) {
			if (!e.hasAttributeNS(NC.text, "style-name")
					&& styleNames.hasStyle("Visited_20_Internet_20_Link",
							"text")) {
				e.setAttributeNS(NC.text, "text:style-name", "Internet_20_link");
			}
			if (!e.hasAttributeNS(NC.text, "visited-style-name")
					&& styleNames.hasStyle("Internet_20_link", "text")) {
				e.setAttributeNS(NC.text, "text:visited-style-name",
						"Visited_20_Internet_20_Link");
			}
		}
	}

	private void setTextLineThroughType(Document doc) {
		XPathResult<Attr> as = XPath
				.attrIterator(
						doc,
						"/*/office:automatic-styles//style:text-properties/@style:text-line-through-style[.='none']");
		for (Attr a : as) {
			final Element e = a.getOwnerElement();
			if (!e.hasAttributeNS(NC.style, "text-line-through-type")) {
				e.setAttributeNS(NC.style, "style:text-line-through-type",
						"none");
			}
		}
	}

	static private void fixBookmarkStart(Document document) {
		// replace each text:bookmark that has a corresponding text:bookmark-end
		// by text:bookmark-start
		XPathResult<Element> elements = XPath.elementIterator(document,
				"//text:bookmark-end");
		Set<String> ends = new HashSet<String>();
		for (Element e : elements) {
			ends.add(e.getAttributeNS(NC.text, "name"));
		}
		elements = XPath.elementIterator(document, "//text:bookmark");
		List<Element> bookmarks = new LinkedList<Element>();
		for (Element e : elements) {
			final String name = e.getAttributeNS(NC.text, "name");
			if (ends.contains(name)) {
				bookmarks.add(e);
			}
		}
		for (Element e : bookmarks) {
			changeQName(e, NC.text, "text:bookmark-start");
		}
	}

	static private void changeQName(Element e, String uri, String qname) {
		final Element e2 = e.getOwnerDocument().createElementNS(uri, qname);
		Node n = e.getFirstChild();
		while (n != null) {
			e2.appendChild(n);
			n = e.getFirstChild();
		}
		final NamedNodeMap as = e.getAttributes();
		final int l = as.getLength();
		for (int i = 0; i < l; ++i) {
			e2.setAttributeNode((Attr) as.item(i).cloneNode(false));
		}
		e.getParentNode().replaceChild(e2, e);
	}

	static private void fixReferenceMarkOrder(Document document, XML xml) {
		XPathResult<Element> elements = XPath
				.elementIterator(
						document,
						"//text:reference-mark-start|//text:reference-mark-end|//text:bookmark|//text:bookmark-start|//text:bookmark-end");
		List<Node> list = new LinkedList<Node>();
		List<Node> done = new LinkedList<Node>();
		for (Element e : elements) {
			if (done.contains(e)) {
				continue;
			}
			done.clear();
			list.clear();
			list.add(e);
			final Node parent = e.getParentNode();
			Node next = e.getNextSibling();
			parent.removeChild(e);
			while (next != null && (xml.isWhitespace(next) || isMark(next))) {
				Node n = next;
				list.add(next);
				done.add(next);
				next = next.getNextSibling();
				parent.removeChild(n);
			}
			Collections.sort(list, referenceMarkSorter);
			for (Node n : list) {
				parent.insertBefore(n, next);
				if (n.getNodeType() == Node.TEXT_NODE) {
					break;
				}
			}
		}
	}

	static private boolean compareSpans(Element span, @Nullable Node n) {
		if (n == null || n.getNodeType() != Node.ELEMENT_NODE) {
			return false;
		}
		Element e = (Element) n;
		if (!span.getNamespaceURI().equals(e.getNamespaceURI())
				|| !span.getLocalName().equals(e.getLocalName())) {
			return false;
		}
		if (!span.getAttributeNS(NC.text, "style-name").equals(
				e.getAttributeNS(NC.text, "style-name"))
				|| !span.getAttributeNS(NC.text, "class-names").equals(
						e.getAttributeNS(NC.text, "class-names"))) {
			return false;
		}
		return true;
	}

	static private void mergeSpans(Document document) {
		XPathResult<Element> spans = XPath.elementIterator(document,
				"//text:span");
		for (Element span : spans) {
			Node n = span.getNextSibling();
			while (compareSpans(span, n)) {
				n.getParentNode().removeChild(n);
				Node c = n.getFirstChild();
				while (c != null) {
					span.appendChild(c);
					c = n.getFirstChild();
				}
				n = span.getNextSibling();
			}
		}
	}

	static private void remove(Node node, String xpath) {
		for (Node n : XPath.nodeIterator(node, xpath)) {
			if (n.getNodeType() == Node.ATTRIBUTE_NODE) {
				Attr a = (Attr) n;
				a.getOwnerElement().removeAttributeNode(a);
			} else {
				n.getParentNode().removeChild(n);
			}
		}
	}

	static final String serialize(Node n) {
		final DOMImplementationLS domImplementation = (DOMImplementationLS) n
				.getOwnerDocument().getImplementation();
		final LSSerializer writer = domImplementation.createLSSerializer();
		writer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE);
		return writer.writeToString(n);
	}

	static final String createName(String xml, MessageDigest digest) {
		digest.reset();
		try {
			digest.update(xml.getBytes("utf8"));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
		byte d[] = digest.digest();
		String hex = String.format("%1$02x%2$02x%3$02x", d[0], d[1], d[2]);
		return hex;
	}
}

interface AttributeFixer {
	String fix(String v);
}

class NormalizeException extends Exception {

	private static final long serialVersionUID = -6951557988831857492L;

	NormalizeException(String msg) {
		super(msg);
	}
}

class IdGroup {
	final String namePrefix;
	final LinkedList<IdAndReferences> ids;
	final Map<String, String> usedNames = new TreeMap<String, String>();
	final boolean removeUnused;

	IdGroup(String namePrefix, LinkedList<IdAndReferences> ids,
			boolean removeUnused) {
		this.namePrefix = namePrefix;
		this.ids = ids;
		this.removeUnused = removeUnused;
	}
}

class IdAndReferenceNormalizer {

	final Document doc;
	final List<IdGroup> list = new LinkedList<IdGroup>();

	IdAndReferenceNormalizer(Document doc) {
		this.doc = doc;
	}

	static private LinkedList<Attr> xPathToList(Document doc, String xpath) {
		LinkedList<Attr> list = new LinkedList<Attr>();
		for (Attr a : XPath.attrIterator(doc, xpath)) {
			list.add(a);
		}
		return list;
	}

	void addIdsAndReferences(String namePrefix, String idsXpath,
			String referencesXPath, boolean removeUnused) {
		LinkedList<Attr> ids = xPathToList(doc, idsXpath);
		LinkedList<Attr> references = xPathToList(doc, referencesXPath);

		LinkedList<IdAndReferences> ids2 = new LinkedList<IdAndReferences>();
		for (Attr a : ids) {
			IdAndReferences id = new IdAndReferences(a, references);
			if (id.references.size() > 0) {
				ids2.add(id);
			} else {
				Element e = a.getOwnerElement();
				e.getParentNode().removeChild(e);
			}
		}
		if (ids2.size() > 0) {
			list.add(new IdGroup(namePrefix, ids2, removeUnused));
		}
	}

	private String idsToString(LinkedList<IdAndReferences> l) {
		String ids = "";
		for (IdAndReferences i : l) {
			ids += i.id.getNodeValue();
		}
		return ids;
	}

	private String listRemainingIdentifiers() {
		String ids = "";
		for (IdGroup g : list) {
			for (IdAndReferences i : g.ids) {
				ids += i.id.getNodeValue() + " (" + idsToString(i.dependencies)
						+ "), ";
			}
		}
		return ids;
	}

	void normalize() throws NormalizeException {
		for (IdGroup g1 : list) {
			for (IdAndReferences i : g1.ids) {
				for (IdGroup g2 : list) {
					for (IdAndReferences j : g2.ids) {
						i.addDependee(j);
					}
				}
			}
		}
		while (list.size() > 0) {
			// find an id with no dependencies
			IdGroup g = null;
			IdAndReferences i = null;
			for (IdGroup g_ : list) {
				for (IdAndReferences i_ : g_.ids) {
					if (i_.dependencies.size() == 0) {
						i = i_;
						g = g_;
						break;
					}
				}
				if (i != null) {
					break;
				}
			}
			if (i == null || g == null) {
				throw new NormalizeException(
						"No id without dependencies can be found. There is a bug or a circular dependency. Remaining ids are "
								+ listRemainingIdentifiers());
			}
			g.ids.remove(i);
			if (g.ids.size() == 0) {
				list.remove(g);
			}
			for (IdAndReferences d : i.dependees) {
				d.dependencies.remove(i);
			}
			normalizeId(g, i, digest);
		}
	}

	private final MessageDigest digest = getMessageDigest();

	static private MessageDigest getMessageDigest() {
		try {
			return MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	static private void updateReferences(IdAndReferences id) {
		String name = id.id.getNodeValue();
		for (Attr a : id.references) {
			a.setNodeValue(name);
		}
	}

	static private void normalizeId(IdGroup group, IdAndReferences id,
			MessageDigest digest) throws NormalizeException {
		final Attr att = id.id;
		final Element e = att.getOwnerElement();
		e.removeAttributeNode(att);
		final String xml = OdfNormalizer.serialize(e);
		final String newName = group.namePrefix
				+ OdfNormalizer.createName(xml, digest);
		att.setNodeValue(newName);
		e.setAttributeNode(att);
		if (group.usedNames.containsKey(newName)) {
			if (!xml.equals(group.usedNames.get(newName))) {
				throw new NormalizeException("Clash in digest for a style.");
			}
			e.getParentNode().removeChild(e);
		} else {
			group.usedNames.put(newName, xml);
		}
		updateReferences(id);
	}
}

class IdAndReferences {
	final Attr id;
	boolean done = false;
	final LinkedList<Attr> references = new LinkedList<Attr>();
	final LinkedList<IdAndReferences> dependencies = new LinkedList<IdAndReferences>();
	final LinkedList<IdAndReferences> dependees = new LinkedList<IdAndReferences>();

	IdAndReferences(Attr id, LinkedList<Attr> r) {
		this.id = id;
		final String name = id.getNodeValue();
		Iterator<Attr> i = r.iterator();
		while (i.hasNext()) {
			Attr a = i.next();
			if (a.getNodeValue().equals(name)) {
				references.add(a);
				i.remove();
			}
		}
	}

	// an id A is a dependee of an id B when
	// - B is a descendant of the element of A
	// - a reference to B is a descendant of the element of A
	// - A is a dependee of a dependee of B
	// the last type of dependency is dealt with separately
	void addDependee(IdAndReferences b) {
		if (b == this) {
			return;
		}
		Element e = id.getOwnerElement();
		if (isDescendant(e, b.id)) {
			b.dependees.add(this);
			this.dependencies.add(b);
		} else {
			for (Attr br : b.references) {
				if (isDescendant(e, br)) {
					b.dependees.add(this);
					this.dependencies.add(b);
					break;
				}
			}
		}
	}

	boolean isDescendant(Element a, Attr b) {
		Element be = b.getOwnerElement();
		while (be != null && be != a) {
			be = getParentElement(be);
		}
		return be == a;
	}

	static public @Nullable Element getParentElement(Element e) {
		Node parent = e.getParentNode();
		return (parent == null || parent.getNodeType() == Node.ELEMENT_NODE) ? (Element) parent
				: null;
	}
}

class IdsAndReferences {

	final LinkedList<Attr> ids;
	final LinkedList<Attr> references;

	IdsAndReferences(LinkedList<Attr> ids, LinkedList<Attr> references) {
		this.ids = ids;
		this.references = references;
	}
}