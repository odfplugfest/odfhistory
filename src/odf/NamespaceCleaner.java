package odf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class NamespaceCleaner {

	static void clean(Document doc) {
		clean(doc.getDocumentElement());
	}

	static void clean(Element e) {
		HashMap<String, String> prefixes = new HashMap<String, String>();
		HashMap<String, String> found = new HashMap<String, String>();
		ArrayList<Attr> toRemove = new ArrayList<Attr>();
		clean(e, prefixes, found, toRemove);
		for (Entry<String, String> entry : prefixes.entrySet()) {
			e.setAttributeNS(NC.xmlns, "xmlns:" + entry.getValue(),
					entry.getKey());
		}
	}

	static private void clean(Element e, HashMap<String, String> prefixes,
			HashMap<String, String> found, ArrayList<Attr> toRemove) {
		String ns = e.getNamespaceURI();
		if (!prefixes.containsKey(ns)) {
			prefixes.put(ns, e.getPrefix());
		}
		final NamedNodeMap atts = e.getAttributes();
		final int l = atts.getLength();
		for (int i = 0; i < l; ++i) {
			final Attr a = (Attr) atts.item(i);
			ns = a.getNamespaceURI();
			if (NC.xmlns.equals(ns)) {
				found.put(a.getLocalName(), a.getValue());
				toRemove.add(a);
			} else {
				if (!prefixes.containsKey(ns)) {
					prefixes.put(ns, a.getPrefix());
				}

				final String ln = a.getLocalName();
				if ((NC.draw.equals(ns) && "engine".equals(ln))
						|| (NC.chart.equals(ns) && "class".equals(ln))
						|| (NC.form.equals(ns) && "control-implementation"
								.equals(ln))
						|| (NC.db.equals(ns) && "type".equals(ln))
						|| (NC.text.equals(ns) && ("condition".equals(ln) || "formula"
								.equals(ln)))) {
					final String value = a.getValue();
					int j = value.indexOf(':');
					if (j != -1) {
						final String prefix = value.substring(0, j);
						final String ns2 = found.get(prefix);
						if (ns2 != null && !prefixes.containsKey(ns2)) {
							prefixes.put(ns2, prefix);
						}
					}
				}
			}
		}
		for (Attr a : toRemove) {
			e.removeAttributeNode(a);
		}
		toRemove.clear();
		Node c = e.getFirstChild();
		while (c != null) {
			if (c.getNodeType() == Node.ELEMENT_NODE) {
				final Element ce = (Element) c;
				clean(ce, prefixes, found, toRemove);
			}
			c = c.getNextSibling();
		}
	}

}
