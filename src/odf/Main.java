package odf;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;

import org.eclipse.jdt.annotation.Nullable;
import org.xml.sax.SAXException;

public class Main {

	public static void main(String[] args) throws IOException, UtilException,
			InterruptedException, SAXException, TransformerException,
			NormalizeException, XMLStreamException {
		// parse the command-line arguments or exit if they make no sense
		RunConfiguration conf;
		try {
			conf = RunConfiguration.parseArguments(args);
		} catch (ArgumentException e) {
			System.err.println(e.getMessage());
			System.exit(1);
			return;
		}
		boolean idle = true;
		OdfZipNormalizer odfNormalizer = new OdfZipNormalizer();
		if (conf.testsDir != null) {
			if (!Tester.runTests(conf.testsDir, odfNormalizer.odfNormalizer)) {
				return;
			}
			idle = false;
		}
		if (conf.input != null && conf.output != null) {
			odfNormalizer.normalize(conf.input, conf.output);
			idle = false;
		}
		if (conf.historyFile != null && conf.repoDir != null
				&& conf.officeDir != null) {
			OdfHistory.create(conf.historyFile, conf.repoDir, conf.officeDir,
					odfNormalizer.odfNormalizer);
			idle = false;
		}
		if (idle) {
			usage();
		}
	}

	private static void usage() {
		final String exe = "odfhistory";

		System.err.println("Usage: " + exe + " --testsDir <testsdir>");
		System.err.println("       " + exe + " --repoDir <repodir> "
				+ "--officeDir <officedir> \\");
		System.err.println("                      --historyFile <historyfile>");
		System.err.println("       " + exe + " <inputfile> <outputfile>");
		System.exit(1);
	}
}

class RunConfiguration {

	@Nullable
	Path testsDir;
	@Nullable
	Path repoDir;
	@Nullable
	Path officeDir;
	@Nullable
	Path historyFile;
	@Nullable
	Path input;
	@Nullable
	Path output;

	static Path stringToWritableDirPath(String path) throws ArgumentException {
		Path p = Paths.get(path).toAbsolutePath();
		if (!Files.isDirectory(p) || !Files.isWritable(p)) {
			throw new ArgumentException("Path " + p
					+ " is not a writable directory.");
		}
		return p;
	}

	static Path stringToReadableDirPath(String path) throws ArgumentException {
		Path p = Paths.get(path).toAbsolutePath();
		if (!Files.isDirectory(p) || !Files.isReadable(p)) {
			throw new ArgumentException("Path " + p
					+ " is not a readable directory.");
		}
		return p;
	}

	static Path stringToReadableFilePath(String path) throws ArgumentException {
		Path p = Paths.get(path).toAbsolutePath();
		if (!Files.isRegularFile(p) || !Files.isReadable(p)) {
			throw new ArgumentException("Path " + p
					+ " is not a readable file.");
		}
		return p;
	}

	static Path stringToWritableFilePath(String path) throws ArgumentException {
		Path p = Paths.get(path).toAbsolutePath();
		Path dir = p.getParent();
		if (!Files.isDirectory(dir) || !Files.isWritable(dir)) {
			throw new ArgumentException("Path " + p
					+ " is not a writable file.");
		}
		return p;
	}

	static RunConfiguration parseArguments(String[] args)
			throws ArgumentException {
		RunConfiguration c = new RunConfiguration();

		int i = 0;
		while (i + 1 < args.length) {
			if (args[i].equals("--testsDir")) {
				c.testsDir = stringToReadableDirPath(args[i + 1]);
			} else if (args[i].equals("--repoDir")) {
				c.repoDir = stringToWritableDirPath(args[i + 1]);
			} else if (args[i].equals("--officeDir")) {
				c.officeDir = stringToReadableDirPath(args[i + 1]);
			} else if (args[i].equals("--historyFile")) {
				c.historyFile = stringToReadableFilePath(args[i + 1]);
			} else {
				c.input = stringToReadableFilePath(args[i]);
				c.output = stringToWritableFilePath(args[i + 1]);
			}
			i += 2;
		}
		return c;
	}
}

class ArgumentException extends Exception {
	private static final long serialVersionUID = 3109890920393877195L;

	ArgumentException(String msg) {
		super(msg);
	}
}