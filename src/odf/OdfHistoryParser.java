package odf;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import javax.xml.transform.TransformerException;

import org.eclipse.jdt.annotation.Nullable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class OdfHistoryParser {
	static LinkedList<OdfHistoryEntry> parse(Path historyFile,
			OdfNormalizer odfNormalizer) throws SAXException, IOException,
			TransformerException {
		FileInputStream in = new FileInputStream(historyFile.toFile());
		odfNormalizer.odfInfo.version = "1.2";
		Document historyDoc = odfNormalizer.xml.parse(in,
				odfNormalizer.odfInfo, null);
		LinkedList<OdfHistoryEntry> files = parseHistory(historyDoc);
		return files;
	}

	static private LinkedList<OdfHistoryEntry> parseHistory(Document doc)
			throws IOException {
		Set<String> paths = new HashSet<String>();
		LinkedList<OdfHistoryEntry> files = new LinkedList<OdfHistoryEntry>();
		XPathResult<Element> rows = XPath
				.elementIterator(doc,
						"/office:document/office:body/office:spreadsheet/table:table/table:table-row");
		for (Element row : rows) {
			final String path = getCellText(row, 1);
			final String dir = getCellText(row, 2);
			final String date = getCellText(row, 3);
			final String target = getCellText(row, 4);
			if ("".equals(path) || ("".equals(dir) && "".equals(target))
					|| "".equals(date)) {
				continue;
			}
			if (paths.contains(path)) {
				throw new IOException("The path " + path + " occurs twice.");
			}
			paths.add(path);
			files.add(new OdfHistoryEntry(path, dir.length() == 0 ? null : dir,
					date, target.length() == 0 ? null : target));
		}
		return files;
	}

	static private String getCellText(Element row, int i) {
		XPathResult<String> text = XPath.stringIterator(row,
				"table:table-cell[" + i + "]/text:p/text()");
		return text.size() > 0 ? text.get(0) : "";
	}
}

class OdfHistoryEntry {
	final String path;
	@Nullable
	final String dir;
	@Nullable
	final String target;
	final String date;

	OdfHistoryEntry(String path, @Nullable String dir, String date,
			@Nullable String target) {
		this.path = path;
		this.dir = dir;
		this.date = date;
		this.target = target;
	}
}