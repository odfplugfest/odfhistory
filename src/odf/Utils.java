package odf;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import org.eclipse.jdt.annotation.Nullable;

public class Utils {
	public static @Nullable Path findAbsolutePath(String exe) {
		String path = System.getenv("PATH");
		String[] dirs = path.split(File.pathSeparator);
		for (String dir : dirs) {
			Path exePath = Paths.get(dir, exe);
			File exeFile = new File(exePath.toString());
			if (exeFile.canExecute())
				return exePath;
		}
		return null;
	}

	public static void cmd(Path workingDir, String exe, String... args)
			throws UtilException, InterruptedException, IOException {
		Path exePath = findAbsolutePath(exe);
		if (exePath == null) {
			throw new UtilException("Cannot find executable '" + exe + "'.");
		}
		String cmd[] = new String[args.length + 1];
		cmd[0] = exePath.toAbsolutePath().toString();
		for (int i = 0; i < args.length; ++i) {
			cmd[i + 1] = args[i];
		}
		ProcessBuilder pb = new ProcessBuilder(cmd);
		pb = pb.directory(workingDir.toFile());
		pb.command(cmd).start().waitFor();
	}

	public static void removeContents(Path path) throws IOException {
		for (Path p : Files.newDirectoryStream(path)) {
			removeRecursive(p);
		}
	}

	public static void removeRecursive(Path path) throws IOException {
		Files.walkFileTree(path, createVisitor(null));
	}

	public static void removeRecursive(Path path, String retainedSuffix)
			throws IOException {
		Files.walkFileTree(path, createVisitor(retainedSuffix));
	}

	private static SimpleFileVisitor<Path> createVisitor(
			@Nullable String retainedSuffix_) {
		final String retainedSuffix = retainedSuffix_;

		return new SimpleFileVisitor<Path>() {
			private void delete(@Nullable Path file) throws IOException {
				if (file == null
						|| retainedSuffix == null
						|| !file.getFileName().toString()
								.endsWith(retainedSuffix)) {
					Files.delete(file);
				}
			}

			@Override
			public FileVisitResult visitFile(@Nullable Path file,
					@Nullable BasicFileAttributes attrs) throws IOException {
				delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(@Nullable Path file,
					@Nullable IOException exc) throws IOException {
				// try to delete the file anyway, even if its attributes
				// could not be read, since delete-only access is
				// theoretically possible
				delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(@Nullable Path dir,
					@Nullable IOException exc) throws IOException {
				if (exc == null) {
					try {
						delete(dir);
					} catch (DirectoryNotEmptyException e) {
					}
					return FileVisitResult.CONTINUE;
				} else {
					// directory iteration failed; propagate exception
					throw exc;
				}
			}
		};
	}
}

class UtilException extends Exception {
	private static final long serialVersionUID = 357885461733959161L;

	UtilException(String msg) {
		super(msg);
	}
}